<%-- 
    Document   : index
    Created on : 23/06/2014, 10:41:24
    Author     : parivero
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Personas</h1>
        <form id="miForm" method="POST" action="/thucydides.demo/persona">
            <input id="nombre" type="text" name="nombre" placeholder="nombre"/>
            <input id="botonFormPersona" type="submit" value="Submit" />
        </form>
        <div id="container">
            <ol id="lista">
            </ol>
        </div>
        <script>

            $(document).ready(function() {
                $('#miForm').submit(function() {
                    $.ajax({
                        type: "POST",
                        url: "/thucydides.demo/persona",
                        data: {nombre: $("#nombre").val()}
                    }).done(function() {
                        listarPersonas();
                    });

                    return false;
                });
                listarPersonas();
            });

            function listarPersonas() {
                $.get("/thucydides.demo/persona", function(data) {
                    if (data != null) {
                        $('#lista').empty();
                        $.each(data, function(index, item) {
                            $('#lista').append('<li>' + item.nombre + '</li>');
                        });
                    }
                })
            }
        </script>
    </body>
</html>

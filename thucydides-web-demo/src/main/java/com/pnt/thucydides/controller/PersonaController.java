/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pnt.thucydides.controller;

import com.pnt.thucydides.domain.Persona;
import com.pnt.thucydides.repository.PersonaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author parivero
 */
@RestController
public class PersonaController {
    
    @Autowired
    private PersonaRepository personaRepository;
    
    @RequestMapping(value = "persona", method = RequestMethod.POST)
    public void alta(Persona persona) {
        personaRepository.save(persona);
    }
    
    @RequestMapping(value = "persona", method = RequestMethod.GET)
    public List<Persona> buscar() {
        return (List<Persona>) personaRepository.findAll();
    }
    
    
}

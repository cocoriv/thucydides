/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pnt.thucydides.repository;

import com.pnt.thucydides.domain.Persona;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author parivero
 */
public interface PersonaRepository extends CrudRepository<Persona, Long>{
    
}

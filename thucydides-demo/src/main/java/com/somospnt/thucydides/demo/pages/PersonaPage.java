package com.somospnt.thucydides.demo.pages;

import ch.lambdaj.function.convert.Converter;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.thucydides.core.pages.WebElementFacade;

import net.thucydides.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

import static ch.lambdaj.Lambda.convert;

@DefaultUrl("http://localhost:8084")
public class PersonaPage extends PageObject {

    @FindBy(name="nombre")
    private WebElementFacade submiteTerms;

    @FindBy(jquery = "#botonFormPersona")
    private WebElementFacade lookupButton;

    public void ingresarNombre(String keyword) {
        submiteTerms.type(keyword);
    }

    public void alta() {
        lookupButton.click();
    }

    public List<String> getPersonas() {
        WebElementFacade definitionList = find(By.id("lista"));
        List<WebElement> results = definitionList.findElements(By.tagName("li"));
        return convert(results, toStrings());
    }

    private Converter<WebElement, String> toStrings() {
        return new Converter<WebElement, String>() {
            public String convert(WebElement from) {
                return from.getText();
            }
        };
    }
}
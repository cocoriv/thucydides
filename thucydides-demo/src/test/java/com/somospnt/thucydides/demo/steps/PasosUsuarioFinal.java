package com.somospnt.thucydides.demo.steps;

import com.somospnt.thucydides.demo.pages.PersonaPage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.fest.assertions.Assertions.assertThat;
import org.fest.assertions.Fail;
import org.junit.Assert;

public class PasosUsuarioFinal extends ScenarioSteps {

    PersonaPage dictionaryPage;

    private static String driver = "org.apache.derby.jdbc.ClientDriver";
    private static String connectionURL = "jdbc:derby://localhost:1527/thucydides;user=test;password=test";
    private static Connection conn;
    private Statement stmt;

    public PasosUsuarioFinal() throws Exception {
        Class.forName(driver);
        conn = DriverManager.getConnection(connectionURL);
        stmt = conn.createStatement();
    }

    @Step
    public void controlNombreDb(String nombre) throws SQLException {
        ResultSet rs = stmt.executeQuery("select nombre from persona where nombre = '" + nombre + "'");
        Assert.assertEquals(nombre, getNombre(rs));
        rs.close();
    }

    @Step
    public void enters(String keyword) {
        dictionaryPage.ingresarNombre(keyword);
    }

    @Step
    public void starts_search() {
        dictionaryPage.alta();
    }

    @Step
    public void deberia_ver_el_nombre(String definition) {
        assertThat(dictionaryPage.getPersonas()).contains(definition);
    }

    @Step
    public void abrir_pagina() {
        dictionaryPage.open();
    }

    @Step
    public void alta(String term) {
        enters(term);
        starts_search();
    }

    @Step
    public String obtenerNombreDb() throws SQLException {
        ResultSet rs = stmt.executeQuery("select nombre from persona FETCH FIRST 1 ROWS ONLY");
        String nombre = getNombre(rs);
        rs.close();
        return nombre;

    }

    private String getNombre(ResultSet rs) throws SQLException {
        String nombre = null;
        while (rs.next()) {
            nombre = rs.getString(1);
        }
        assertThat(nombre);
        return nombre;

    }

}

package com.somospnt.thucydides.demo;

import com.somospnt.thucydides.demo.requirements.Application;
import com.somospnt.thucydides.demo.steps.PasosUsuarioFinal;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.runners.ThucydidesRunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.sql.SQLException;

@Story(Application.AltaPersona.AltaMultiplesPersonas.class)
@RunWith(ThucydidesRunner.class)
public class AltaPersonaStoryTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://localhost:8084/thucydides.demo")
    public Pages pages;

    @Steps
    public PasosUsuarioFinal usuarioFinal;

    @Test
    public void altaPersona_conNombrePepe() throws SQLException {
        String nombre = "pepe";
        usuarioFinal.abrir_pagina();
        usuarioFinal.alta(nombre);
        usuarioFinal.deberia_ver_el_nombre(nombre);
        usuarioFinal.controlNombreDb(nombre);

    }

    @Test
    public void altaPersona_conNombreToto() throws SQLException {
        String nombre = "tito";
        usuarioFinal.abrir_pagina();
        usuarioFinal.alta(nombre);
        usuarioFinal.deberia_ver_el_nombre(nombre);
        usuarioFinal.controlNombreDb(nombre);
    }

    @Test
    public void altaPersona_conNombreExistenteMasPrefijo() throws SQLException {
        String nombre = usuarioFinal.obtenerNombreDb();
        String nombrePref = "test_"+nombre;
        usuarioFinal.abrir_pagina();
        usuarioFinal.alta(nombrePref);
        usuarioFinal.deberia_ver_el_nombre(nombrePref);
        usuarioFinal.controlNombreDb(nombrePref);
    }



}
